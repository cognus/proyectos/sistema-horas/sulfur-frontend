import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogHoraDetalleComponent } from './dialog-hora-detalle.component';

describe('DialogHoraDetalleComponent', () => {
  let component: DialogHoraDetalleComponent;
  let fixture: ComponentFixture<DialogHoraDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogHoraDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogHoraDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
